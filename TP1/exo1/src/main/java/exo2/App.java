package exo2;

public class App {

	public static String scytale(String mess, int w) {
		
		int ligne=mess.length()/w;
		if (ligne * w != mess.length()){
			ligne++;
		}
		char[][] sol = new char[w][ligne];
		
		for (int i=0; i <w; i++){
			for (int j=0; j <ligne; j++){

				if (j*w+i < mess.length())
					sol[i][j] = mess.charAt(j*w+i);
				else
					sol[i][j]=' ';
			}
		}

		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<w ; i++){
			for(int j = 0; j<ligne ; j++){
				sb.append(sol[i][j]);
			}
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(scytale("lelnracsrunanatuvllerrmcnjeeaeseiaetanctsagsgeemftqdnnentraraueneciliianeredofaesntdneenignpcdaishdcaoaeenede", 11));
	}

}
