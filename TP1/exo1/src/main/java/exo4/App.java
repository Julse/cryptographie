package exo4;

import java.util.ArrayList;
import java.util.TreeSet;

public class App {

    /*SOLUTION Q1 : ZOLA*/
    /*SOLUTION Q2 : ATHOS*/

    /* En vrai plein d'opti pour faire les bons tests, choisir les bonnes structures faire des coupures en fonction
    des probas d'apparition de suite de lettre, puis tester tous les multiples des patterns trouvées, filtrer etc..
    complexe quoi.....
     */
	public static String lettres = "abcdefghijklmnopqrstuvwxyz";

	public static TreeSet<Integer> ploygram(String mess) {
		TreeSet<Integer> patterns = new TreeSet(); 
		int size = 4;
		for(int i=0; i<mess.length()-size; i++)
		{
			String pattern = mess.substring(i, i+size);
			if(mess.substring(i+size).contains(pattern)){
				System.out.println("p1 " + i + "  -> " + mess.substring(i, i+size));
				System.out.println(mess.substring(i+size).indexOf(pattern));
				patterns.add(mess.substring(i+size).indexOf(pattern));
			}
		}
		
		return patterns;
	}

    public static TreeSet<Mot> combinaisons = new TreeSet<Mot>();

	public static String polyAlpha(String mess, String cle){

		StringBuilder sb = new StringBuilder();

		for (int i=0; i<mess.length(); i++)
		{
            if (cle.charAt(i%cle.length()) == ' '){
                sb.append(' ');
            }
            else {
                char l = mess.charAt(i);
                char c = cle.charAt(i % cle.length());
                int d = lettres.indexOf(c);
                char ttt = lettres.charAt(((lettres.indexOf(l) - d) + 26) % 26);

                sb.append(ttt);
            }

			//System.out.println("-> NEW :" + ttt);
		}

		return sb.toString();
	}


	public static void generate(int n, Mot perm){

        Mot motCopy = perm.Copy();
		if (!combinaisons.contains(motCopy)){
			combinaisons.add(motCopy);
		}

		if (n == 1){
			return;
		}
		else {
			for (int i = 0; i < n - 1; i ++){
				generate(n - 1, perm);
				if (n%2==0){
					char permut = perm.getMot()[i];
					perm.getMot()[i] = perm.getMot()[n-1];
					perm.getMot()[n-1]=permut;
				}
					else{
						char permut = perm.getMot()[0];
						perm.getMot()[0]=perm.getMot()[n-1];
						perm.getMot()[n-1]=permut;
					}
			}
			generate(n - 1, perm);
		}
	}

    public static void main(String[] args) {
		String code = "zbpuevpuqsdlzgllksousvpasfpddggaqwptdgptzweemqzrdjtddefekeferdprrcyndgluaowcnbptzzzrbvpssfpashpncotemhaeqrferdlrlwwertlussfikgoeuswotfdgqsyasrlnrzppdhtticfrciwurhcezrpmhtpuwiyenamrdbzyzwelzucamrptzqseqcfgdrfrhrpatsepzgfnaffisbpvblisrplzgnemswaqoxpdseehbeeksdptdttqsdddgxurwnidbdddplncsd";

        String code2 = "iefomntuohenwfwsjbsfftpgsnmhzsbbizaomosiuxycqaelrwsklqzekjvwsivijmhuvasmvwjewlzgubzlavclhgmuhwhakookakkgmrelgeefvwjelksedtyhsgghbamiyweeljcemxsohlnzujagkshakawwdxzcmvkhuwswlqwtmlshojbsguelgsumlijsmlbsixuhsdbysdaolfatxzofstszwryhwjenuhgukwzmshbagigzzgnzhzsbtzhalelosmlasjdttqzeswwwrklfguzl";

        //TreeSet<Integer> patterns = ploygram(code);
        TreeSet<Integer> patterns = ploygram(code2);

        for (Integer i : patterns){
            System.out.println("PATTERN " + i);
        }

        int multiple = 0;
		for (Integer integ : patterns){
            ArrayList<Integer> multiples = getMultiples(integ);
            //Juste pour le test!
            if (multiples.size()>=3) {
                multiple = multiples.get(2);
            }

            System.out.println("Multiples de : " + integ);
            for (Integer i : multiples){
                System.out.println(i);
            }
		}

        multiple = 5;
        String codeTest = code.substring(0,88);
        String codeTest2 = code2.substring(0,88);
        // Tout ca pour ce final foireux!
        for (int c1=0; c1<lettres.length(); c1++) {
            for (int c2 = 0; c2 < lettres.length(); c2++) {
                for (int c3 = 0; c3 < lettres.length(); c3++) {
                    for (int c4 = 0; c4 < lettres.length(); c4++) {
                        //for (int c5 = 0; c5 < lettres.length(); c5++) {
                            char[] decalage = new char[multiple];
                            decalage[0] = lettres.charAt(c1);
                            decalage[1] = lettres.charAt(c2);
                            decalage[2] = lettres.charAt(c3);
                            decalage[3] = lettres.charAt(c4);
                            decalage[4] = ' ';
                            //for (int i=0; i<multiple; i++){
                            // decalage[i]=lettres.charAt(i);
                            // Mot decal = new Mot(decalage,decalage.length);
                            // generate(4,decal);
                            //}
                            Mot decal = new Mot(decalage, decalage.length);
                            combinaisons.add(decal);
                        //}
                    }
               }
            }
        }

        System.out.println("Combi: " + combinaisons.size());
		System.out.println("Ouch : " + 26*26*26*26);

        //System.out.println("CODETest :" + codeTest.length());
        System.out.println("CODETest :" + codeTest2.length());
        //TreeSet<Mot> solutions = new TreeSet<Mot>();
        for (Mot m : combinaisons) {
            //System.out.println(m.print());
            //System.out.println(polyAlpha(code, m.print()));
            String mot = polyAlpha(codeTest2, m.print());
            if        ((mot.split("z").length < 2) && (mot.split("w").length < 2)
                    && (mot.split("x").length < 3) && (mot.split("k").length < 3)
                    && (mot.split("q").length < 4) && (mot.split("y").length < 4)
                    && (mot.split("h").length < 5) && (mot.split("j").length < 5)
                    && (mot.split("f").length < 6) && (mot.split("g").length < 6)
                    && (mot.split("p").length < 7) && (mot.split("gf").length < 2)) {
                //System.out.println("un truc");

                //solutions.add(new Mot(mot, codeTest.length()));
                //System.out.println(mot);
                //System.out.println("solution : " + m);
            }
        }

        System.out.println("SOLUTION? : " + polyAlpha(codeTest2, "athos"));

    }

    private static ArrayList<Integer> getMultiples(Integer integ) {
        ArrayList<Integer> multiples = new ArrayList<Integer>();
        for (int i=1; i<integ; i++){
            if (integ%i ==0){
                multiples.add(i);
            }
        }
        return multiples;
    }

}
