package exo4;

import java.util.HashMap;
import java.util.Map;


public class LaBelleLangueFrancaise {

    Map<String, Double> bigrammes;
    Map<String, Double> monogrammes;
    double nope;

    public LaBelleLangueFrancaise() {

        // Probabilité des bigrammes (sur la feuilles de TD)
        // Je met en log pour éviter de manipuler des valeurs trop petites et de se faire avoir sur une erreure d'approx

        bigrammes = new HashMap<String, Double>();
        monogrammes = new HashMap<String, Double>();

        bigrammes.put("es", Math.log10(305.0/10000.0));
        bigrammes.put("te", Math.log10(163.0/10000.0));
        bigrammes.put("ou", Math.log10(118.0/10000.0));
        bigrammes.put("ec", Math.log10(100.0/10000.0));
        bigrammes.put("eu", Math.log10(89.0/10000.0));
        bigrammes.put("ep", Math.log10(82.0/10000.0));
        bigrammes.put("le", Math.log10(246.0/10000.0));
        bigrammes.put("se", Math.log10(155.0/10000.0));
        bigrammes.put("ai", Math.log10(117.0/10000.0));
        bigrammes.put("ti", Math.log10(98.0/10000.0));
        bigrammes.put("ur", Math.log10(88.0/10000.0));
        bigrammes.put("nd", Math.log10(80.0/10000.0));
        bigrammes.put("en", Math.log10(242.0/10000.0));
        bigrammes.put("et", Math.log10(143.0/10000.0));
        bigrammes.put("em", Math.log10(113.0/10000.0));
        bigrammes.put("ce", Math.log10(98.0/10000.0));
        bigrammes.put("co", Math.log10(87.0/10000.0));
        bigrammes.put("ns", Math.log10(79.0/10000.0));
        bigrammes.put("de", Math.log10(215.0/10000.0));
        bigrammes.put("el", Math.log10(141.0/10000.0));
        bigrammes.put("it", Math.log10(112.0/10000.0));
        bigrammes.put("ed", Math.log10(96.0/10000.0));
        bigrammes.put("ar", Math.log10(86.0/10000.0));
        bigrammes.put("pa", Math.log10(78.0/10000.0));
        bigrammes.put("re", Math.log10(209.0/10000.0));
        bigrammes.put("qu", Math.log10(134.0/10000.0));
        bigrammes.put("me", Math.log10(104.0/10000.0));
        bigrammes.put("ie", Math.log10(94.0/10000.0));
        bigrammes.put("tr", Math.log10(86.0/10000.0));
        bigrammes.put("us", Math.log10(76.0/10000.0));
        bigrammes.put("nt", Math.log10(197.0/10000.0));
        bigrammes.put("an", Math.log10(30.0/10000.0));
        bigrammes.put("is", Math.log10(103.0/10000.0));
        bigrammes.put("ra", Math.log10(92.0/10000.0));
        bigrammes.put("ue", Math.log10(85.0/10000.0));
        bigrammes.put("sa", Math.log10(75.0/10000.0));
        bigrammes.put("on", Math.log10(164.0/10000.0));
        bigrammes.put("ne", Math.log10(124.0/10000.0));
        bigrammes.put("la", Math.log10(101.0/10000.0));
        bigrammes.put("in", Math.log10(90.0/10000.0));
        bigrammes.put("ta", Math.log10(85.0/10000.0));
        bigrammes.put("ss", Math.log10(73.0/10000.0));
        bigrammes.put("er", Math.log10(163.0/10000.0));
        bigrammes.put("nt", Math.log10(197.0/10000.0));
        bigrammes.put("tr", Math.log10(86.0/10000.0));
        bigrammes.put("ns", Math.log10(79.0/10000.0));
        bigrammes.put("st", Math.log10(61.0/10000.0));
        bigrammes.put("ou", Math.log10(118.0/10000.0));
        bigrammes.put("ai", Math.log10(117.0/10000.0));
        bigrammes.put("ie", Math.log10(94.0/10000.0));
        bigrammes.put("eu", Math.log10(89.0/10000.0));
        bigrammes.put("ue", Math.log10(85.0/10000.0));
        bigrammes.put("ui", Math.log10(68.0/10000.0));
        bigrammes.put("au", Math.log10(64.0/10000.0));
        bigrammes.put("oi", Math.log10(52.0/10000.0));
        bigrammes.put("io", Math.log10(49.0/10000.0));
        bigrammes.put("ss", Math.log10(73.0/10000.0));
        bigrammes.put("ee", Math.log10(66.0/10000.0));
        bigrammes.put("ll", Math.log10(29.0/10000.0));
        bigrammes.put("tt", Math.log10(24.0/10000.0));
        bigrammes.put("nn", Math.log10(20.0/10000.0));
        bigrammes.put("mm", Math.log10(17.0/10000.0));
        bigrammes.put("rr", Math.log10(16.0/10000.0));
        bigrammes.put("pp", Math.log10(10.0/10000.0));
        bigrammes.put("ff", Math.log10(8.0/10000.0));
        bigrammes.put("cc", Math.log10(3.0/10000.0));


        monogrammes.put("a", Math.log10(0.07636));
        monogrammes.put("b", Math.log10(0.00901));
        monogrammes.put("c", Math.log10(0.03260));
        monogrammes.put("d", Math.log10(0.03669));
        monogrammes.put("e", Math.log10(0.14715));
        monogrammes.put("f", Math.log10(0.01066));
        monogrammes.put("g", Math.log10(0.00866));
        monogrammes.put("h", Math.log10(0.00737));
        monogrammes.put("i", Math.log10(0.07529));
        monogrammes.put("j", Math.log10(0.00545));
        monogrammes.put("k", Math.log10(0.00049));
        monogrammes.put("l", Math.log10(0.05456));
        monogrammes.put("m", Math.log10(0.0298));
        monogrammes.put("n", Math.log10(0.07095));
        monogrammes.put("o", Math.log10(0.05378));
        monogrammes.put("p", Math.log10(0.03021));
        monogrammes.put("q", Math.log10(0.01362));
        monogrammes.put("r", Math.log10(0.06553));
        monogrammes.put("s", Math.log10(0.07948));
        monogrammes.put("t", Math.log10(0.07244));
        monogrammes.put("u", Math.log10(0.06311));
        monogrammes.put("v", Math.log10(0.01628));
        monogrammes.put("w", Math.log10(0.00114));
        monogrammes.put("x", Math.log10(0.00387));
        monogrammes.put("y", Math.log10(0.00308));
        monogrammes.put("z", Math.log10(0.00136));

        nope = Math.log10(0.01/10000.0);
    }

    public double confiance(String phrase) {

        // Technique sympa vu sur le web :
        // P(ELLE) = P(EL) * P(LL) * P(LE)
        // log(P(ELLE)) = log(P(EL)) + log(P(LL)) + log(P(LE))
        //
        // On somme le log des probas de chaque couple de lettres dans le message

        double score = 0;
        for(int i=0; i<phrase.length()-2; i++) {
            if(bigrammes.containsKey(phrase.substring(i, i+2))) {
                score += bigrammes.get(phrase.substring(i, i+2));
            } else {
                score += nope;
            }
            if (monogrammes.containsKey(phrase.substring(i,i+1))){
                score += monogrammes.get(phrase.substring(i, i+1));
            }
            else{
                score += nope;
            }
        }
        return score;
    }
}
