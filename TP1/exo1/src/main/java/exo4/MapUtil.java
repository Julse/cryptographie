package exo4;

import java.util.*;

// Source : http://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values-java

// Pour trier une map selon ses valeurs
public class MapUtil
{
    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue( Map<K, V> map )
    {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return -1*(o1.getValue()).compareTo( o2.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }
    /**
     * Method sortByKey.
     * @param map Map<K,V>
     * @return Map<K,V>
     */
    public static <K extends Comparable<? super K>, V> Map<K, V> sortByKey(
            final Map<K, V> map) {
        @SuppressWarnings("unchecked")
        final Map.Entry<K, V>[] array = map.entrySet().toArray(
                new Map.Entry[map.size()]);

        Arrays.sort(array, new Comparator<Map.Entry<K, V>>() {

            public int compare(final Map.Entry<K, V> e1,
                               final Map.Entry<K, V> e2) {
                return e1.getKey().compareTo(e2.getKey());
            }
        });

        final Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : array) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}