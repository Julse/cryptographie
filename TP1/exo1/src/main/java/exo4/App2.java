package exo4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class App2 {
	
	public static String lettres = "abcdefghijklmnopqrstuvwxyz";

    // Fait des "stats" sur les diviseurs de nombres
    // diviseur -> nombre d'occurence
	public static void analyseDiviseurs(int nb, Map<Integer, Integer> stats) {
        for (int i=nb; i>1; i--) {
            if (nb%i == 0) {
                //System.out.print(i + " ");
                if (stats.containsKey(i)) {
                    stats.put(i, stats.get(i) + 1);
                } else {
                    stats.put(i, 1);
                }
            }
        }
    }

    // Cherche les longeurs de clé les plus probables
	public static ArrayList<Integer> ploygram(String mess) {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
        Map<Integer, Integer> test = new HashMap<Integer, Integer>();

		for(int size = 2; size <5; size++) {
            for (int i = 0; i < mess.length() - size; i++) {
                String pattern = mess.substring(i, i + size);
                if (mess.substring(i + size).contains(pattern)) {
                    analyseDiviseurs(mess.substring(i + size).indexOf(pattern) + size, test);
                }
            }
        }

        // Range les taille de clé possible de la plus probable à la moins probable
        //test = MapUtil.sortByKey(test);
        test = MapUtil.sortByValue(test);

        // Garde les 5 plus probables
        int c = 5;
        for(Map.Entry<Integer, Integer> entry : test.entrySet()) {
            if (c>0) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
                sizes.add(entry.getKey());
                c--;
            } else {
                break;
            }
        }

		return sizes;
	}

    // Decrypte un chiffré de Vigenere
	public static String polyAlpha(String mess, String cle){
		
		StringBuilder sb = new StringBuilder();
		
		for (int i=0; i<mess.length(); i++)
		{
			char l = mess.charAt(i);
			char c = cle.charAt(i%cle.length());
			int  d = lettres.indexOf(c);
			char ttt = lettres.charAt(((lettres.indexOf(l)-d)+26)%26);
			
			//System.out.println("-> NEW :" + ttt);
			
			sb.append(ttt);
		}
		
		return sb.toString();
	}

    // Obtient la forme textuelle d'une clé (à partir d'un tableau de 0..25
    public static String makeKey(int[] cle) {
        StringBuilder sb = new StringBuilder();

        for(Integer i : cle)
            sb.append(lettres.charAt(i));

        return sb.toString();
    }

    // Teste toutes les combinaisons de clé de taille 'size'
    // Affiche le résultat le plus probable
    public static void bruteForce(String mess, int size) {

        LaBelleLangueFrancaise fr = new LaBelleLangueFrancaise();

        // On commence a tester la cle aaaa...aaa
        int[] cle = new int[size];
        for(int i=0; i<size; i++)
            cle[i] = 0;
        int[] tmp = cle.clone();

        double best = fr.confiance(polyAlpha(mess, makeKey(tmp)));

        // On va tester les combinaisons possibles et retenir la clé la plus probable
        int pos = 0;
        int streak = size;
        boolean change = false;
        while(streak > 0) {
            for(int z=0; z<26; z++) {
                tmp[pos%size] = (tmp[pos%size] + 1)%26;
                double ts = fr.confiance(polyAlpha(mess, makeKey(tmp)));
                if (ts > best) {
                    best = ts;
                    cle = tmp.clone();
                    change = true;
                    break;
                }
            }
            pos += 1;
            if (!change) {
                streak--;
            } else {
                streak = size;
                change = false;
            }
        }

        // Affichage
        System.out.println(makeKey(cle));
        System.out.println(polyAlpha(mess, makeKey(cle)));
    }
	
	public static void main(String[] args) {

        String mess = "zbpuevpuqsdlzgllksousvpasfpddggaqwptdgptzweemqzrdjtddefekeferdprrcyndgluaowcnbptzzzrbvpssfpashpncotemhaeqrferdlrlwwertlussfikgoeuswotfdgqsyasrlnrzppdhtticfrciwurhcezrpmhtpuwiyenamrdbzyzwelzucamrptzqseqcfgdrfrhrpatsepzgfnaffisbpvblisrplzgnemswaqoxpdseehbeeksdptdttqsdddgxurwnidbdddplncsd";
        String mess2= "iefomntuohenwfwsjbsfftpgsnmhzsbbizaomosiuxycqaelrwsklqzekjvwsivijmhuvasmvwjewlzgubzlavclhgmuhwhakookakkgmrelgeefvwjelksedtyhsgghbamiyweeljcemxsohlnzujagkshakawwdxzcmvkhuwswlqwtmlshojbsguelgsumlijsmlbsixuhsdbysdaolfatxzofstszwryhwjenuhgukwzmshbagigzzgnzhzsbtzhalelosmlasjdttqzeswwwrklfguzl";


        System.out.println("Q1 :");
        ArrayList<Integer> sizes = ploygram(mess);
        System.out.println("Best guess : ");
        for(Integer s : sizes)
            bruteForce(mess, s);


        System.out.println("Q2 :");
        ArrayList<Integer> sizes2 = ploygram(mess2);
        System.out.println("Best guess : ");
        for(Integer s : sizes2)
            bruteForce(mess2, s);
    }

}
