package exo4;

/**
 * Created by Jules on 17/03/2016.
 */
public class StringTest {

    public static void main(String[] args) {
        String test = "123456123";
        System.out.println(test.substring(3));
        System.out.println(test.substring(3).indexOf("123"));

        int pos =0;
        int[] tmp = new int[4];
        tmp[0] = 0;
        tmp[1] = 0;
        tmp[2] = 0;
        tmp[3] = 0;

        for(int z=0; z<26; z++) {
            tmp[0] = (tmp[0] + 1)%26;
            System.out.println("je suis passé : " + z +" fois.");
            System.out.println(tmp[0]);
        }
        System.out.println("fin: " + tmp[0]);
    }
}
