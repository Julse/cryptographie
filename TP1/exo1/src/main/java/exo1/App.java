package exo1;

/**
 * Hello world!
 *
 */
public class App 
{
	public static String Cesar(String mess, int dec) {
		
		String lettres = "abcdefghijklmnopqrstuvwxyz";
		
		StringBuilder sb = new StringBuilder();
		
		for(int i=0; i<mess.length(); i++)
		{
			int idx = lettres.indexOf(mess.charAt(i));
			int idxChar=idx+dec;
			if (idxChar < 0){
				idxChar+=26;
			}
			sb.append(lettres.charAt((idxChar)%26));
		}
		
		
		return sb.toString();
	}
	
    public static void main( String[] args )
    {
    	// Chaine 1
    	System.out.println( Cesar("vcfgrwqwfsbhfsntowbsobgfsbhfsnqvsnjcigsghqsoixcifrviwtshseicwbsgojsnjcigdogeisjcigoihfsgofhwgobgjcigbsrsjsnqwfqizsfrobgzsgfisgzsgxcifgcijfopzsgeiojsqzsggwubsgrsjchfsdfctsggwcbdofzseiszsghhcbashwsf", -14) );
    	
    	// Chaine 2
    	System.out.println( Cesar("hcihszouoizssghrwjwgsssbhfcwgdofhwsgrcbhzibssghvopwhssdofzsgpszusgzoihfsdofzsgoeiwhowbgzohfcwgwsasdofqsileiwrobgzsifzobuisgsbcaasbhqszhsgshrobgzobchfsuoizcwg", -14) );
    }
}
