package exo3;

public class App {
	
	public static String affine(String mess, int a, int b) 
	{
		String lettres = "abcdefghijklmnopqrstuvwxyz";
	
		StringBuilder sb = new StringBuilder();
		
		for(int i =0; i<mess.length(); i++) {
			int y = lettres.indexOf(mess.charAt(i));
			int sol = ((((y-b)+26)%26)*(26-a))%26;
			
			sb.append(lettres.charAt(sol));
		}
		
		return sb.toString();
	}

	public static void main(String[] args) {
		/*Q1*/
		System.out.println(affine("ntjmpumgxpqtstgqpgtxpnchumtputgfsftgthnngxnchumwxootrtumhpyctgktjqtjchfooxujqhgztumxpotjxotfoqtohrxumhzutwftgtopfmntjmpuatmfmshodpfrxpjjtqtghbxuj", 17, 7));

		/*Q2*/
		// TODO Auto-generated method stub
		for(int a=1; a<26; a++)
		{
			for(int b=0; b<26; b++)
			{
				System.out.println("a="+a+" b="+b);
				System.out.println(affine("spaxhnnvjupkytpcpuppycxklppygpkpycxkyejkpapzzphktvkkjppyrjpsxhzppyrjhzxhzppyrjhkpnytwxrjpavhnkhyvjyxaxhyoxzpzpkhyvjyxaxhyjkpxjycppyzxhzppyzptvzgcpke", a,b));
				System.out.println("");
			}
		}		
	}

}
