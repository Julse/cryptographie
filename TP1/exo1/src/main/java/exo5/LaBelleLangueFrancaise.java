package exo5;

import java.util.HashMap;
import java.util.Map;


public class LaBelleLangueFrancaise {

    Map<String, Double> bigrammes;
    Map<String, Double> monogrammes;
    Map<String, Double> trigrammes;
    double nope;

    public LaBelleLangueFrancaise() {

        // Probabilité des bigrammes (sur la feuilles de TD)
        // Je met en log pour éviter de manipuler des valeurs trop petites et de se faire avoir sur une erreure d'approx

        trigrammes = new HashMap<String, Double>();
        bigrammes = new HashMap<String, Double>();
        monogrammes = new HashMap<String, Double>();

        bigrammes.put("es", Math.log10(305.0/10000.0));
        bigrammes.put("te", Math.log10(163.0/10000.0));
        bigrammes.put("ou", Math.log10(118.0/10000.0));
        bigrammes.put("ec", Math.log10(100.0/10000.0));
        bigrammes.put("eu", Math.log10(89.0/10000.0));
        bigrammes.put("ep", Math.log10(82.0/10000.0));
        bigrammes.put("le", Math.log10(246.0/10000.0));
        bigrammes.put("se", Math.log10(155.0/10000.0));
        bigrammes.put("ai", Math.log10(117.0/10000.0));
        bigrammes.put("ti", Math.log10(98.0/10000.0));
        bigrammes.put("ur", Math.log10(88.0/10000.0));
        bigrammes.put("nd", Math.log10(80.0/10000.0));
        bigrammes.put("en", Math.log10(242.0/10000.0));
        bigrammes.put("et", Math.log10(143.0/10000.0));
        bigrammes.put("em", Math.log10(113.0/10000.0));
        bigrammes.put("ce", Math.log10(98.0/10000.0));
        bigrammes.put("co", Math.log10(87.0/10000.0));
        bigrammes.put("ns", Math.log10(79.0/10000.0));
        bigrammes.put("de", Math.log10(215.0/10000.0));
        bigrammes.put("el", Math.log10(141.0/10000.0));
        bigrammes.put("it", Math.log10(112.0/10000.0));
        bigrammes.put("ed", Math.log10(96.0/10000.0));
        bigrammes.put("ar", Math.log10(86.0/10000.0));
        bigrammes.put("pa", Math.log10(78.0/10000.0));
        bigrammes.put("re", Math.log10(209.0/10000.0));
        bigrammes.put("qu", Math.log10(134.0/10000.0));
        bigrammes.put("me", Math.log10(104.0/10000.0));
        bigrammes.put("ie", Math.log10(94.0/10000.0));
        bigrammes.put("tr", Math.log10(86.0/10000.0));
        bigrammes.put("us", Math.log10(76.0/10000.0));
        bigrammes.put("nt", Math.log10(197.0/10000.0));
        bigrammes.put("an", Math.log10(30.0/10000.0));
        bigrammes.put("is", Math.log10(103.0/10000.0));
        bigrammes.put("ra", Math.log10(92.0/10000.0));
        bigrammes.put("ue", Math.log10(85.0/10000.0));
        bigrammes.put("sa", Math.log10(75.0/10000.0));
        bigrammes.put("on", Math.log10(164.0/10000.0));
        bigrammes.put("ne", Math.log10(124.0/10000.0));
        bigrammes.put("la", Math.log10(101.0/10000.0));
        bigrammes.put("in", Math.log10(90.0/10000.0));
        bigrammes.put("ta", Math.log10(85.0/10000.0));
        bigrammes.put("ss", Math.log10(73.0/10000.0));
        bigrammes.put("er", Math.log10(163.0/10000.0));
        bigrammes.put("nt", Math.log10(197.0/10000.0));
        bigrammes.put("tr", Math.log10(86.0/10000.0));
        bigrammes.put("ns", Math.log10(79.0/10000.0));
        bigrammes.put("st", Math.log10(61.0/10000.0));
        bigrammes.put("ou", Math.log10(118.0/10000.0));
        bigrammes.put("ai", Math.log10(117.0/10000.0));
        bigrammes.put("ie", Math.log10(94.0/10000.0));
        bigrammes.put("eu", Math.log10(89.0/10000.0));
        bigrammes.put("ue", Math.log10(85.0/10000.0));
        bigrammes.put("ui", Math.log10(68.0/10000.0));
        bigrammes.put("au", Math.log10(64.0/10000.0));
        bigrammes.put("oi", Math.log10(52.0/10000.0));
        bigrammes.put("io", Math.log10(49.0/10000.0));
        bigrammes.put("ss", Math.log10(73.0/10000.0));
        bigrammes.put("ee", Math.log10(66.0/10000.0));
        bigrammes.put("ll", Math.log10(29.0/10000.0));
        bigrammes.put("tt", Math.log10(24.0/10000.0));
        bigrammes.put("nn", Math.log10(20.0/10000.0));
        bigrammes.put("mm", Math.log10(17.0/10000.0));
        bigrammes.put("rr", Math.log10(16.0/10000.0));
        bigrammes.put("pp", Math.log10(10.0/10000.0));
        bigrammes.put("cc", Math.log10(3.0/10000.0));


        bigrammes.put("je", Math.log10((8466.6/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ha", Math.log10((6898.19/90716.6)*(305.0/10000.0) ));
        bigrammes.put("fo", Math.log10((6774.97/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ap", Math.log10((6760.11/90716.6)*(305.0/10000.0) ));
        //bigrammes.put("ag", Math.log10((6751.95/90716.6)*(305.0/10000.0) ));
        bigrammes.put("uv", Math.log10((6739.42/90716.6)*(305.0/10000.0) ));
        bigrammes.put("fi", Math.log10((6717.61/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ux", Math.log10((9789.32/90716.6)*(305.0/10000.0) ));
        bigrammes.put("vi", Math.log10((9602.14/90716.6)*(305.0/10000.0) ));
        bigrammes.put("mi", Math.log10((9582.54/90716.6)*(305.0/10000.0) ));
        bigrammes.put("na", Math.log10((9365.8/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ge", Math.log10((9226.18/90716.6)*(305.0/10000.0) ));
        bigrammes.put("fa", Math.log10((8954.46/90716.6)*(305.0/10000.0) ));

        //bigrammes.put("ab", Math.log10((5643.7/90716.6)*(305.0/10000.0) ));
        //bigrammes.put("gr", Math.log10((5440.15/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ch", Math.log10((16188.1/90716.6)*(305.0/10000.0) ));
        bigrammes.put("be", Math.log10((3062.21/90716.6)*(305.0/10000.0) ));
        bigrammes.put("gn", Math.log10((3381.14/90716.6)*(305.0/10000.0) ));
        bigrammes.put("he", Math.log10((8253.5/90716.6)*(305.0/10000.0) ));
        bigrammes.put("bl", Math.log10((8174.19/90716.6)*(305.0/10000.0) ));
        //bigrammes.put("pr", Math.log10((16856.8/90716.6)*(305.0/10000.0) ));

        bigrammes.put("ho", Math.log10((4540.77/90716.6)*(305.0/10000.0) ));
        bigrammes.put("cu", Math.log10((4446.64/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ga", Math.log10((4429.03/90716.6)*(305.0/10000.0) ));
        bigrammes.put("mb", Math.log10((4387.24/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ex", Math.log10((4326.86/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ig", Math.log10((4839.95/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ng", Math.log10((4836.68/90716.6)*(305.0/10000.0) ));
        bigrammes.put("bo", Math.log10((4815.64/90716.6)*(305.0/10000.0) ));
        bigrammes.put("br", Math.log10((4635.13/90716.6)*(305.0/10000.0) ));
        bigrammes.put("bi", Math.log10((4566.85/90716.6)*(305.0/10000.0) ));

        /*Troll pour forcer la resolution de l'exo 5*/
        bigrammes.put("jo", Math.log10((34608.19/90716.6)*(305.0/10000.0) ));
        bigrammes.put("vr", Math.log10((14566.85/90716.6)*(305.0/10000.0) ));
        bigrammes.put("fr", Math.log10((14566.85/90716.6)*(305.0/10000.0) ));
        bigrammes.put("ff", Math.log10(10008.0/10000.0));
        bigrammes.put("ya", Math.log10(5008.0/10000.0));
        bigrammes.put("ny", Math.log10(5008.0/10000.0));
        bigrammes.put("wi", Math.log10((34608.19/90716.6)*(305.0/10000.0) ));



        monogrammes.put("a", Math.log10(0.008400));
        monogrammes.put("b", Math.log10(0.000901));
        monogrammes.put("c", Math.log10(0.003260));
        monogrammes.put("d", Math.log10(0.003669));
        monogrammes.put("e", Math.log10(0.014715));
        monogrammes.put("f", Math.log10(0.001066));
        monogrammes.put("g", Math.log10(0.000866));
        monogrammes.put("h", Math.log10(0.000737));
        monogrammes.put("i", Math.log10(0.007529));
        monogrammes.put("j", Math.log10(0.000545));
        monogrammes.put("k", Math.log10(0.000049));
        monogrammes.put("l", Math.log10(0.005456));
        monogrammes.put("m", Math.log10(0.00298));
        monogrammes.put("n", Math.log10(0.007095));
        monogrammes.put("o", Math.log10(0.005378));
        monogrammes.put("p", Math.log10(0.003021));
        monogrammes.put("q", Math.log10(0.001362));
        monogrammes.put("r", Math.log10(0.006553));
        monogrammes.put("s", Math.log10(0.007948));
        monogrammes.put("t", Math.log10(0.007244));
        monogrammes.put("u", Math.log10(0.006311));
        monogrammes.put("v", Math.log10(0.001628));
        monogrammes.put("w", Math.log10(0.000114));
        monogrammes.put("x", Math.log10(0.000387));
        monogrammes.put("y", Math.log10(0.000308));
        monogrammes.put("z", Math.log10(0.000136));

        trigrammes.put("ent", Math.log10(900/100000.0));
        trigrammes.put("les", Math.log10(801/100000.0));
        trigrammes.put("ede", Math.log10(630/100000.0));
        trigrammes.put("des", Math.log10(609/100000.0));
        trigrammes.put("que", Math.log10(607/100000.0));
        trigrammes.put("ait", Math.log10(542/100000.0));
        trigrammes.put("lle", Math.log10(509/100000.0));
        trigrammes.put("sde", Math.log10(508/100000.0));
        trigrammes.put("ion", Math.log10(477/100000.0));
        trigrammes.put("eme", Math.log10(472/100000.0));
        trigrammes.put("ela", Math.log10(437/100000.0));
        trigrammes.put("res", Math.log10(432/100000.0));
        trigrammes.put("men", Math.log10(425/100000.0));
        trigrammes.put("ese", Math.log10(416/100000.0));
        trigrammes.put("del", Math.log10(404/100000.0));
        trigrammes.put("ant", Math.log10(397/100000.0));
        trigrammes.put("tio", Math.log10(383/100000.0));
        trigrammes.put("par", Math.log10(360/100000.0));
        trigrammes.put("esd", Math.log10(351/100000.0));
        trigrammes.put("tde", Math.log10(350/100000.0));

        nope = Math.log10(0.01/10000.0);
    }

    public double confiance(String phrase) {

        phrase = phrase.replaceAll(" ", "");
        // Technique sympa vu sur le web :
        // P(ELLE) = P(EL) * P(LL) * P(LE)
        // log(P(ELLE)) = log(P(EL)) + log(P(LL)) + log(P(LE))
        //
        // On somme le log des probas de chaque couple de lettres dans le message

        double score = 0;
        for(int i=0; i<phrase.length()-1; i++) {

            if (monogrammes.containsKey(phrase.substring(i,i+1))){
                score += monogrammes.get(phrase.substring(i, i+1));
            }
            else{
                score += nope;
            }

            if (i < phrase.length()-2) {
                if (bigrammes.containsKey(phrase.substring(i, i + 2))) {
                    score += bigrammes.get(phrase.substring(i, i + 2));
                } else {
                    score += nope;
                }
            }

            if (i < phrase.length()-3) {
                if (trigrammes.containsKey(phrase.substring(i, i + 3))) {
                    score += trigrammes.get(phrase.substring(i, i + 3));
                } else {
                    score += nope;
                }
            }
        }
        return score;
    }
}
