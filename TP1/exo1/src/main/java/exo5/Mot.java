package exo5;

import java.util.Arrays;

/**
 * Created by Jules on 16/03/2016.
 */
public class Mot implements Comparable{

    public char[] mot;
    public int length;

    public Mot(){}

    public Mot(String mot, int length){
        this.length=length;
        this.mot=new char[length];
        for (int i=0; i<length;i++){
            this.mot[i]=mot.charAt(i);
        }
    }

    public Mot(char[] motCopy, int length) {
        this.mot = motCopy;
        this.length = length;
    }

    public int compareTo(Object o) {
        Mot motToCompare = (Mot) o;
        if (motToCompare.length != mot.length) {
            return -1;
        }
        for (int i = 0 ; i<length; i++){
            if (mot[i] != motToCompare.getMot()[i]) {
                return -1;
            }
        }
        return 0;
    }

    public char[] getMot() {
        return mot;
    }

    public void setMot(char[] mot) {
        this.mot = mot;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Mot Copy() {
        char[] motCopy =  new char[length];
        for (int i =0; i<length; i++){
            motCopy[i]=mot[i];
        }

        return new Mot(motCopy, length);
    }

    @Override
    public String toString() {
        return "Mot{" +
                "mot=" + Arrays.toString(mot) +
                '}';
    }

    public String print() {
        String res="";
        for (int i=0; i<length; i++)
            res+=mot[i];
        return res;
    }
}
