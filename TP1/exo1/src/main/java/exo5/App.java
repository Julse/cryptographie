package exo5;

import exo4.MapUtil;
import exo4.Mot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created by Jules on 17/03/2016.
 */
public class App {
    /*Alphabet*/
    public static String lettres = "abcdefghijklmnopqrstuvwxyz";
    /*Texte exo 5*/
    public static String code1 = "v ubcfb osu ymoqsuu n cxqfj dqmfnu ub vjcfqu juz amqjmruz zmsscfusb bquflu auoquz hfszbms zwfba\n" +
                                "ju wusbms qusbqu ncsz ju vmo z uddmqvcfb n uxfbuq ju xusb wcoxcfz fj eczzc qcefnuwusb jc emqbu\n" +
                                "xfbquu no ijmv nuz wcfzmsz nu jc xfvbmfqu ecz czzul qcefnuwusb vueusncsb emoq uweuvauq kou z\n" +
                                "usrmoddqu us wuwu buwez kou jof os bmoqifjjms nu emozzfuqu ub nu zciju\n" +
                                "ju acjj zusbcfb ju vamo vofb ub ju xfuog bcefz c j osu nu zuz ugbquwfbuz osu cddfvau nu vmojuoq\n" +
                                "bqme xczbu emoq vu nuejmfuwusb fsbuqfuoq ubcfb vjmouu co woq ujju quequzusbcfb zfwejuwusb os\n" +
                                "usmqwu xfzcru jcqru nu ejoz n os wubqu ju xfzcru n os amwwu n usxfqms kocqcsbu vfsk csz c j\n" +
                                "uecfzzu wmozbcvau smfqu cog bqcfbz cvvusbouz ub iucog\n" +
                                "hfszbms zu nfqfruc xuqz j uzvcjfuq fj ubcfb fsobfju n uzzcpuq nu equsnqu j czvuszuoq wuwu cox\n" +
                                "wufjjuoquz uemkouz fj dmsvbfmsscfb qcquwusb cvboujjuwusb n cfjjuoqz ju vmoqcsb ujuvbqfkou ubcfb\n" +
                                "vmoeu ncsz jc ymoqsuu v ubcfb osu nuz wuzoquz n uvmsmwfu eqfzuz us xou nu jc zuwcfsu nu jc acfsu\n" +
                                "zms ceecqbuwusb ubcfb co zuebfuwu hfszbms kof cxcfb bqusbu suod csz ub zmoddqcfb n os ojvuqu\n" +
                                "xcqfkouog co nuzzoz nu jc vauxfjju nqmfbu wmsbcfb jusbuwusb fj z cqqubc ejozfuoqz dmfz us vauwfs\n" +
                                "emoq zu quemzuq c vackou ecjfuq zoq osu cddfvau vmjjuu co woq dcvu c jc vcru nu j czvuszuoq j\n" +
                                "usmqwu xfzcru xmoz dfgcfb no qurcqn v ubcfb os nu vuz emqbqcfbz cqqcsruz nu bujju zmqbu kou juz\n" +
                                "puog zuwijusb zofxqu vujof kof eczzu osu jurusnu zmoz ju emqbqcfb nfzcfb ifr iqmbauq xmoz qurcqnu\n" +
                                "c j fsbuqfuoq nu j ceecqbuwusb nu hfszbms osu xmfg zovquu dcfzcfb usbusnqu osu zuqfu nu smwiquz\n" +
                                "kof cxcfusb bqcfb c jc eqmnovbfms nu jc dmsbu jc xmfg eqmxuscfb n osu ejckou nu wubcj mijmsrou\n" +
                                "wfqmfq buqsu usvczbqu ncsz ju woq nu nqmfbu hfszbms bmoqsc os imobms ub jc xmfg nfwfsoc nu xmjowu\n" +
                                "wcfz juz wmbz ubcfusb usvmqu nfzbfsvbz ju zms nu j ceecqufj no bujuvqcs vmwwu ms nfzcfb emoxcfb\n" +
                                "ubqu czzmoqnf wcfz fj s p cxcfb covos wmpus nu j ubufsnqu vmwejubuwusb hfszbms zu nfqfruc xuqz jc\n" +
                                "dusubqu fj ubcfb nu zbcboqu dquju ejobmb eubfbu ub zc wcfrquoq ubcfb zmojfrsuu ecq jc vmwifscfzms\n" +
                                "ijuou osfdmqwu no ecqbf fj cxcfb juz vauxuog bquz ijmsnz ju xfzcru scboqujjuwusb zcsrofs jc euco\n" +
                                "noqvfu ecq ju zcxms rqmzzfuq juz jcwuz nu qczmfq uwmozzuuz ub ju dqmfn nu j afxuq kof xuscfb nu\n" +
                                "equsnqu d";


    public static TreeSet<Mot> combinaisons = new TreeSet<Mot>();

    public static void generate(int n, Mot perm){

        exo4.Mot motCopy = perm.Copy();
        if (!combinaisons.contains(motCopy)){
            combinaisons.add(motCopy);
        }

        if (n == 1){
            return;
        }
        else {
            for (int i = 0; i < n - 1; i ++){
                generate(n - 1, perm);
                if (n%2==0){
                    char permut = perm.getMot()[i];
                    perm.getMot()[i] = perm.getMot()[n-1];
                    perm.getMot()[n-1]=permut;
                }
                else{
                    char permut = perm.getMot()[0];
                    perm.getMot()[0]=perm.getMot()[n-1];
                    perm.getMot()[n-1]=permut;
                }
            }
            generate(n - 1, perm);
        }
    }

    /*substitution mono-alphabetique*/
    public static Map<Integer, Integer> substitution = new HashMap<Integer,Integer>();
    public static String bruteForce(String mess) {

        exo5.LaBelleLangueFrancaise fr = new exo5.LaBelleLangueFrancaise();

        //On trouve d'abord le e

        Map<Integer,Integer> occurences = new HashMap<Integer, Integer>();
        for (int i=0; i< lettres.length(); i++)
            occurences.put(i,0);

        for (char c : mess.toCharArray()){
            if (lettres.contains(String.valueOf(c))) {
                Integer value = occurences.get(lettres.indexOf(c));
                occurences.put(lettres.indexOf(c), value + 1);
            }
        }

        occurences = MapUtil.sortByValue(occurences);
        System.out.println("****** Occurences: ******");
        ArrayList<Integer> sortedOccurences = new ArrayList<Integer>();
        for (Integer key : occurences.keySet()){
            System.out.println(lettres.charAt(key) +" : " + occurences.get(key));
            sortedOccurences.add(key);
        }

        ArrayList<Integer> monogrammes = new ArrayList<Integer>();
        monogrammes.add(4);monogrammes.add(18);monogrammes.add(0);monogrammes.add(8);monogrammes.add(19);
        monogrammes.add(13);monogrammes.add(17);monogrammes.add(20);monogrammes.add(11);monogrammes.add(14);
        monogrammes.add(3);monogrammes.add(2);monogrammes.add(15);monogrammes.add(12);monogrammes.add(21);
        monogrammes.add(16);monogrammes.add(5);monogrammes.add(9);monogrammes.add(1);monogrammes.add(6);
        monogrammes.add(7);monogrammes.add(22);monogrammes.add(23);monogrammes.add(24);monogrammes.add(25);
        monogrammes.add(10);


        System.out.println("***** Initialisation par substitution par occurences *****");
        for (int i=0; i<sortedOccurences.size(); i++) {
            int idxCur = monogrammes.get(i);
            System.out.println(lettres.charAt(sortedOccurences.get(i)) + " : " + lettres.charAt(idxCur));
            substitution.put(sortedOccurences.get(i), idxCur);
        }

        String alphabetFinal = new String(lettres);
        int idxE = 4;
        alphabetFinal = alphabetFinal.replace(String.valueOf(lettres.charAt(idxE)),"");
        String alphabetCopy = new String(alphabetFinal);

        String firstTry = monoAlpha(mess, substitution);
        System.out.println("");
        System.out.println("*** Premiere substitution par occurences ***");
        System.out.println(firstTry);
        System.out.println("");

        double best = fr.confiance(firstTry);
        boolean change = false;

        int streak = lettres.length();
        int pos=0;
        int sizeS=lettres.length();
        while (streak>0){
            if (pos%substitution.size()==0){
                alphabetCopy=new String(alphabetFinal);
            }
            for(int z=0; z<alphabetCopy.length(); z++) {
                int idxNewLetter = lettres.indexOf(alphabetCopy.charAt(z));

                //GENERER LES BONNES PERMUTATIONS!!
                char cTmp = alphabetCopy.charAt(pos%alphabetCopy.length());
                int idxLetterCur = lettres.indexOf(cTmp);

                //On fait la permutation

                int idxValue1 = idxLetterCur;
                int valueToSave1 = substitution.get(idxLetterCur);

                int idxValue2 = 0;
                for (Integer key : substitution.keySet()){
                    if (substitution.get(key) == idxNewLetter){
                        idxValue2 = key;
                    }
                }
                int valueToSave2 = idxNewLetter;


                substitution.put(idxLetterCur, idxNewLetter);
                substitution.put(idxValue2, valueToSave1);

                /*int idxToChange= substitution.get(idxNewLetter);
                while (idxToChange != idxLetterCur){
                    rollback.put(idxNewLetter,idxToChange);
                    idxNewLetter = idxToChange;
                    idxToChange = substitution.get(idxToChange);
                    substitution.put(idxNewLetter,);
                }*/

                //substitution.get(pos%sizeS) + 1) % lettres.length());
                //alphabetCopy += alphabetCopy.replace(lettres.charAt((substitution.get(pos%sizeS))%lettres.length()),' ');
                //lettres.indexOf(lettres.charAt((substitution.get(pos%sizeS) + 1)%lettres.length())%lettres.length()));

                double ts = fr.confiance(monoAlpha(mess, substitution));
                if (ts > best) {
                    best = ts;
                    change = true;
                    //AlphabetFinal
                    alphabetCopy = alphabetCopy.replace(String.valueOf(lettres.charAt(idxNewLetter)),"");
                    break;
                }
                else {
                    substitution.put(idxValue1, valueToSave1);
                    substitution.put(idxValue2, valueToSave2);
                }
            }
            pos += 1;
            if (!change) {
                streak--;
            } else {
                streak = sizeS;
                change = false;
            }
        }

         return monoAlpha(mess, substitution);
    }

    private static String monoAlpha(String mess, Map<Integer, Integer> substitution) {
        Map<Integer, ArrayList<Integer>> replacement = new HashMap<Integer, ArrayList<Integer>>();

        String messCopy = new String(mess);
        for (int i=0; i<lettres.length(); i++){
            if (substitution.containsKey(i)) {
                if (substitution.get(i) != i) {
                    messCopy = new String(mess);
                    messCopy = messCopy.replace(lettres.charAt(i), lettres.charAt(substitution.get(i)));
                    ArrayList<Integer> positions = new ArrayList<Integer>();
                    for (int j = 0; j < messCopy.length(); j++) {
                        if (mess.charAt(j) != messCopy.charAt(j)) {
                            positions.add(j);
                        }
                    }
                    replacement.put(i, positions);
                }
            }
        }

        /*
        System.out.println("*** Affichage remplacement ***");
        for (Integer k : replacement.keySet()){
            System.out.print("Position de " + lettres.charAt(k) + " : ");
            for (Integer l : replacement.get(k)){
                System.out.print(l + " ");
            }
            System.out.println("");
        }*/

        StringBuilder messBuilder = new StringBuilder(messCopy);
        TreeSet<Integer> replaced = new TreeSet<Integer>();

        for (int i=0; i<messBuilder.length(); i++){
            int idxC = lettres.indexOf(mess.charAt(i));
            if      (replacement.containsKey(idxC)
                    && !replaced.contains(idxC)){
                replaced.add(idxC);
                for (Integer c : replacement.get(idxC)){
                    messBuilder.setCharAt(c, lettres.charAt(substitution.get(idxC)));
                }
            }
        }
        return messBuilder.toString();
    }

    public static void main(String[] args) {
        //testMono();
        System.out.println("*** EXERCICE 5 ***");
        System.out.println("Test de remplacement mono-alphabétique (une lettre par une autre lettre)");

        System.out.println("***Message à décoder :***");
        System.out.println(code1);

        // Affichage
        System.out.println("");
        System.out.println("***Résultat final :***");
        System.out.println(bruteForce(code1));

        substitution= MapUtil.sortByKey(substitution);
        System.out.println("");
        System.out.println("*Récapitulatif des substitutions:*");
        for (Integer key : substitution.keySet()){
            System.out.print(lettres.charAt(key) + " ");
        }
        System.out.println("");
        for (Integer value : substitution.values()){
            System.out.print(lettres.charAt(value) + " ");
        }
        //System.out.println(combinaisons.size());
    }


    private static void testMono() {
        /*Premier test sur monoalpha*/
        String code = "aaaaaaaabbbbbbbccccccdddddeeeefffggh";
        ArrayList<Integer> monogrammes = new ArrayList<Integer>();
        monogrammes.add(4);monogrammes.add(18);monogrammes.add(0);monogrammes.add(8);monogrammes.add(19);
        monogrammes.add(13);monogrammes.add(17);monogrammes.add(20);monogrammes.add(11);monogrammes.add(14);
        monogrammes.add(3);monogrammes.add(2);monogrammes.add(15);monogrammes.add(12);monogrammes.add(21);
        monogrammes.add(16);monogrammes.add(5);monogrammes.add(9);monogrammes.add(1);monogrammes.add(6);
        monogrammes.add(7);monogrammes.add(22);monogrammes.add(23);monogrammes.add(24);monogrammes.add(25);
        monogrammes.add(10);

        for (int i=0; i<lettres.length(); i++) {
            int idxCur = monogrammes.get(i);
            substitution.put(i, idxCur);
        }

        System.out.println(monoAlpha(code,substitution));
        System.out.println(code);
    }
}

        /*Une possiblité : découper l'alphabet pour pouvoir générer les permutations*/
        /*Mot m1 = new Mot(lettres.substring(0,7), 7);
        generate(7,m1);
        Mot m2 = new Mot(lettres.substring(7,14), 7);
        generate(7,m2);
        Mot m3 = new Mot(lettres.substring(14,20), 6);
        generate(6,m3);
        Mot m4 = new Mot(lettres.substring(20,26), 6);
        generate(6,m4);*/

//a b c d e f g h i j k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
//0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
        /*Brute force a la main pour finir :/*/
        /*
        substitution.put(24,9); //j
        substitution.put(11,25); //z
        substitution.put(15,24); //y
        substitution.put(24,9); //j
        substitution.put(0,7); //h
        substitution.put(7,22); //x
        substitution.put(8,1); //b
        substitution.put(17,6); //g
        substitution.put(6,23); //x
        */
