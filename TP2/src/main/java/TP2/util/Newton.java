package TP2.util;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Newton {

	private static final BigInteger TWO = new BigInteger("2");
	private static final BigInteger THREE = new BigInteger("3");

	public static BigInteger cqrt(BigInteger m) {

		int diff=m.compareTo(BigInteger.ZERO);
		if (diff==0) return BigInteger.valueOf(0);

		BigDecimal two=new BigDecimal(TWO);
		BigDecimal three=new BigDecimal(THREE);


		BigDecimal n=new BigDecimal(m);
		//Begin with an initial guess-the cubic root will be third the size of m
		//Make a byte array at least that long, & set bit in the high order byte
		byte[] barray=new byte[m.bitLength()/24+1];
		barray[0]=(byte)255;

		//This is the first guess-it will be too high
		BigDecimal r=new BigDecimal(new BigInteger(1,barray));

		//Next approximation is computed by taking r-f(r)/f'(r)
		r=r.subtract((r.pow(3).subtract(n)).divide((r.pow(2)).multiply(three),BigDecimal.ROUND_UP));

		//As long as our new approximation squared exceeds m, we continue to approximate
		while ((r.pow(3)).compareTo(n)>0) {
		  r=r.subtract((r.pow(3).subtract(n)).divide((r.pow(2)).multiply(three),BigDecimal.ROUND_UP));
	      }
	      return r.toBigInteger();
	    }
}
