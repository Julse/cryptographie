package TP2.elgamal;

import EVALUATION.Keys;
import EVALUATION.PrimeUtil;

import java.math.BigInteger;
import java.util.Random;

/**
 * Hello world!
 *
 */
public class TestElgamal 
{
    public static void main( String[] args )
    {
        Elgamal elgamal = new Elgamal();
        Keys keys = elgamal.generateKeys();

        System.out.println("Entier non crypté : " + 10);
        BigInteger[] crypt = elgamal.encrypt(keys.getPublicKey(), new BigInteger("10"));
        System.out.println("Entier crypté : " + crypt[1]);

        BigInteger decrypt = elgamal.decrypt(crypt, keys.getPrivateKey());
        System.out.println("Entier decrypté : " + decrypt);
    }
}
