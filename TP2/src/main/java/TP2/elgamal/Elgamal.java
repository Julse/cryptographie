package TP2.elgamal;

import EVALUATION.Keys;
import EVALUATION.PrimeUtil;
import EVALUATION.PrivateKey;
import EVALUATION.PublicKey;

import java.math.BigInteger;
import java.util.Random;

// Q1 : cardinal = p-1; ordres = les diviseurs de p-1; 2*+1
// Q2 : 

public class Elgamal {

	private final Random rand = new Random();
	private final int certainty = 50;

	/**
	 * @return la génération aléatoire d'une clé publique et d'une clé privée comme suit :
	 * (a) tirer aleatoirement un premier p a l'aide de la methode getPrime. Celui-ci est de la forme p = 2p'+1
	 * avec p' premier,
	 * (b) tirer aleatoirement un generateur de (Z/pZ)* d'ordre 2*p'!! ;
	 * (c) tirer aleatoirement un entier x de [0; p' - 1] ;
	 * (d) calculer h = g^x dans Z/pZ;
	 * (e) la clef secrete est le couple (p; x), la clef publique est (p; g; h).
	 */
	public Keys generateKeys() {
		BigInteger p = PrimeUtil.getPrime(128, certainty, rand); //(a)
		BigInteger pprime = p.subtract(BigInteger.ONE).divide(new BigInteger("2"));
		BigInteger g = PrimeUtil.getGeneratorOfZpZ(p);//(b) d'ordre p' : impossible, sinon ce n'est pas un générateur!!

		BigInteger x = PrimeUtil.randNum(pprime,certainty, rand);

		BigInteger h = g.modPow(x,p);

		BigInteger[] publicKey = new BigInteger[3];
		publicKey[0] = p;
		publicKey[1] = g;
		publicKey[2] = h;
		PublicKey publicKey1 = new PublicKey(publicKey);
		BigInteger[] privateKey = new BigInteger[2];
		privateKey[0]= p;
		privateKey[1]= x;
		PrivateKey privateKey1 = new PrivateKey(privateKey);

		return new Keys(publicKey1, privateKey1);
	};


	/**
	 * @param publicKey la clé publique à chiffrer
	 * @param toCrypt l'entier a crypter
	 * @return une paire (g^r;m*h^r)
	 */
	public BigInteger[] encrypt(PublicKey publicKey, BigInteger toCrypt) {
		BigInteger[] paire = new BigInteger[2];

		BigInteger[] publicKey1 = publicKey.getPublicKey();
		BigInteger p = publicKey1[0];
		BigInteger g = publicKey1[1];
		BigInteger h = publicKey1[2];
		BigInteger pprime = p.subtract(BigInteger.ONE).divide(new BigInteger("2"));

		BigInteger r = PrimeUtil.randNum(pprime,certainty,rand);

		paire[0] = g.modPow(r,p);
		paire[1] = toCrypt.multiply(h.modPow(r, p));

		return paire;
	}

	/**
	 * @param paire l'entier a décrypter
	 * @return l'entier dechiffre
	 */
	public BigInteger decrypt (BigInteger[] paire, PrivateKey secretKey) {
		BigInteger gr = paire[0];
		BigInteger p = secretKey.getPrivateKey()[0];
		return paire[1].multiply((gr.modPow(secretKey.getPrivateKey()[1], p).modInverse(p))).mod(p);
	}
	
}