package TP2.rsa;

import EVALUATION.Keys;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class TestRSA {

	static int bitlength = 2048;
    static Random rand = new Random();
    static RSA rsa = new RSA();

    
    public static void TP4() {
    	
    	
    }
    
    public static void main( String[] args ) {
        //displayRSAMenu();
        //doChoice();
    	TP4();
    }

    private static void doChoice() {
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();

            switch (choice){
                case 1:
                    String message = getMessage();
                    doRSA(message, RSAStatus.None);
                    break;
                case 2:
                    message = getMessage();
                    doRSA(message, RSAStatus.Opti);
                    break;
                case 3:
                    message = getMessage();
                    doRSA(message, RSAStatus.Salt);
                    break;
                case 4:
                    sc = new Scanner(System.in);
                    System.out.print("Rentrer un entier à crypter: ");
                    String messageEntier = "" + sc.nextInt();
                    System.out.println();
                    sc = new Scanner(System.in);
                    //GENERATION DE CLE
                    System.out.print("Rentrer une longueur de bit pour le codage (1024, 2048, 4096): ");
                    bitlength = sc.nextInt();
                    System.out.println();

                    bigAttack(messageEntier);
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Au revoir");
                    break;
            }
        System.out.println();
        displayRSAMenu();
        doChoice();
    }

    private static String getMessage() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Rentrer un message à crypter: ");
        String message = sc.nextLine();
        System.out.println();
        return message;
    }


    public static void displayRSAMenu(){
        System.out.println("***************************************");
        System.out.println("***            RSA TEST             ***");
        System.out.println("***************************************");
        System.out.println();
        System.out.println("        1. RSA");
        System.out.println("        2. RSA Optimisé");
        System.out.println("        3. RSA avec sécurité XOR");
        System.out.println("        4. Test d'une attaque sur RSA non sécurisé");
        System.out.println("        5. Quitter");
        System.out.println();
        System.out.println("***************************************");
        System.out.println();
    }

    
    public static void doRSA(String message, RSAStatus rsaStatus){

        Scanner sc = new Scanner(System.in);
        String messageCrypte = "";
        ArrayList<BigInteger> messageCrypteTab = new ArrayList<BigInteger>();
    	rand = new Random();

        //Pour les mesures de temps
        long a;
        long b;

        //GENERATION DE CLE
        System.out.print("Rentrer une longueur de bit pour le codage (1024, 2048, 4096): ");
        bitlength = sc.nextInt();
        System.out.println();

        Keys keys = RSA.generateKeys(rsaStatus, bitlength, message);


        //CRYPTAGE
        a = System.currentTimeMillis();
        for (Character c : message.toCharArray()) {
            int cHash = c;
            String mess = "" + cHash;

            BigInteger crypt = null;
            switch (rsaStatus){
                case None:
                    crypt = RSA.encrypt(keys.getPublicKey(), new BigInteger(mess));
                    break;
                case Opti:
                    crypt = RSA.encrypt(keys.getPublicKey(), new BigInteger(mess));
                    break;
                case Salt:
                    crypt = RSA.encryptWithSalt(keys.getPublicKey(), new BigInteger(mess));
                    break;
            }

            messageCrypteTab.add(crypt);
            messageCrypte += crypt.toString();
        }
        b = System.currentTimeMillis();

        System.out.println("Message crypté : " + messageCrypte);
        System.out.println("En " + (b - a) + " ms");

        //DECRYPTAGE
        String messDecrypte = "";

        a = System.currentTimeMillis();
        for (BigInteger bi : messageCrypteTab) {

            switch (rsaStatus){
                case None:
                    BigInteger decrypt = RSA.decrypt(bi, keys.getPrivateKey());
                    messDecrypte += new Character((char) decrypt.intValue());
                    break;
                case Opti:
                    decrypt = RSA.decrypt_CRT(bi, keys.getPrivateKey());
                    messDecrypte += new Character((char) decrypt.intValue());
                    break;
                case Salt:
                    decrypt = RSA.decryptWithSalt(bi, keys.getPrivateKey());
                    messDecrypte += new Character((char) decrypt.intValue());
                    break;
            }
        }
        b = System.currentTimeMillis();
        switch (rsaStatus){
            case None:
                System.out.println("Message decrypté sans opti : " + messDecrypte);
                break;
            case Opti:
                System.out.println("Message decrypté avec opti : " + messDecrypte);
                break;
            case Salt:
                System.out.println("Message decrypté avec salt : " + messDecrypte);
                break;
        }
        System.out.println("En " + (b - a) + " ms");
    
    }
    
    
    public static void bigAttack(String message){
    	
    	 rand = new Random();
    	
    	 Keys keysBob1 = rsa.generateKeys(bitlength, true);
         Keys keysBob2 = rsa.generateKeys(bitlength, true);
         Keys keysBob3 = rsa.generateKeys(bitlength, true);

         BigInteger c1 = rsa.encrypt(keysBob1.getPublicKey(), new BigInteger(message));
         System.out.println("Entier c1 : " + c1.toString());
         BigInteger c2 = rsa.encrypt(keysBob2.getPublicKey(), new BigInteger(message));
         System.out.println("Entier c2 : " + c2.toString());
         BigInteger c3 = rsa.encrypt(keysBob3.getPublicKey(), new BigInteger(message));
         System.out.println("Entier c3 : " + c3.toString());
         
         BigInteger N1 = keysBob1.getPublicKey().getPublicKey()[0];
         BigInteger N2 = keysBob2.getPublicKey().getPublicKey()[0];
         BigInteger N3 = keysBob3.getPublicKey().getPublicKey()[0];

         System.out.println("Entier décodé aprees BigAttack : " + RSA.bigAttack(c1, c2, c3, N1, N2, N3));
    }
}
