package TP2.rsa;

import java.math.BigInteger;
import java.util.Random;

import EVALUATION.Keys;
import EVALUATION.PrimeUtil;
import EVALUATION.PrivateKey;
import EVALUATION.PublicKey;
import TP2.util.Newton;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by Jules on 12/04/2016.
 */
public class RSA {
    private static final Random rand = new Random();
    private static final int certainty = 50;

    public static Keys generateKeys(RSAStatus rsaStatus, int bitlength, String message) {
        Keys keys = null;
        switch (rsaStatus){
            case None:
                keys = RSA.generateKeys(bitlength, false);
                break;
            case Opti:
                keys = RSA.generateKeys(bitlength, false);
                break;
            case Salt:
                keys = RSA.generateKeysWithSalt(bitlength, false, new BigInteger(message));
                break;
            default:
                break;
        }
        return keys;
    }


    public static Keys generateKeys(int bitlength, boolean forced) {
        BigInteger p = PrimeUtil.getPrime(bitlength/2, certainty, rand);
        BigInteger q = PrimeUtil.getPrime(bitlength/2, certainty, rand);

        BigInteger pq = p.multiply(q);
        
        while (pq.bitLength() != bitlength){
            p = PrimeUtil.getPrime(bitlength/2, certainty, rand);
            q = PrimeUtil.getPrime(bitlength/2, certainty, rand);

            pq = p.multiply(q);
        }


        BigInteger phin = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE)); 
        
        if (forced){
	        while (!(phin.gcd(new BigInteger("3")).equals(BigInteger.ONE))){
	            p = PrimeUtil.getPrime(bitlength/2, certainty, rand);
	            q = PrimeUtil.getPrime(bitlength/2, certainty, rand);
	
	            pq = p.multiply(q);
	            
	            phin = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
	        }
        }
        //fonction indicatrice d'euler de pq

        int phibitlength = phin.bitLength();
        
        int randbitlength = (rand.nextInt()%phibitlength);

        while (randbitlength < 2){
            randbitlength = (rand.nextInt()%phibitlength);
        }
        
        BigInteger e = PrimeUtil.getPrime(randbitlength,certainty,rand);


        while (phin.subtract(e).signum() != 1  || !phin.gcd(e).equals(BigInteger.ONE) || randbitlength < 2){
            randbitlength = (rand.nextInt()%phibitlength);
            e = PrimeUtil.getPrime(randbitlength,certainty,rand);
        }
        
        if (forced){
        	e = new BigInteger("3");
        }
        //d, inverse de e modulo φ(n)
        BigInteger d = e.modInverse(phin);
        BigInteger n = pq;

        /*Le couple (n,e) est la clé publique du chiffrement,
        alors que le nombre d est sa clé privée, sachant que l'opération de déchiffrement
        ne demande que la clef privée d et l'entier n, connu par la clé publique (la clé privée est parfois
        aussi définie comme le triplet (d, p, q).*/
        BigInteger[] publicKey = new BigInteger[2];
        publicKey[0] = n;
        publicKey[1] = e;
        PublicKey publicKey1 = new PublicKey(publicKey);

        BigInteger[] privateKey = new BigInteger[3];
        privateKey[0]= d;
        privateKey[1]= p;
        privateKey[2]= q;
        PrivateKey privateKey1 = new PrivateKey(privateKey);

        return new Keys(publicKey1, privateKey1);
    }
    
    public static Keys generateKeysWithSalt(int bitlength, boolean forced, BigInteger mess) {
    	
        BigInteger p = PrimeUtil.getPrime(bitlength/2, certainty, rand);
        BigInteger q = PrimeUtil.getPrime(bitlength/2, certainty, rand);
        BigInteger pq = p.multiply(q);
        while (pq.bitLength() != bitlength){
            p = PrimeUtil.getPrime(bitlength/2, certainty, rand);
            q = PrimeUtil.getPrime(bitlength/2, certainty, rand);
            pq = p.multiply(q);
        }


        BigInteger phin = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
        //fonction indicatrice d'euler de pq
        int phibitlength = phin.bitLength();
        
        int randbitlength = (rand.nextInt()%phibitlength);
        while (randbitlength < 2){
            randbitlength = (rand.nextInt()%phibitlength);
        }
        
        BigInteger e = PrimeUtil.getPrime(randbitlength,certainty,rand);

        while (phin.subtract(e).signum() != 1  || !phin.gcd(e).equals(BigInteger.ONE) || randbitlength < 2){
            randbitlength = (rand.nextInt()%phibitlength);
            e = PrimeUtil.getPrime(randbitlength,certainty,rand);
        }

        //d, inverse de e modulo φ(n)
        BigInteger d = e.modInverse(phin);
        BigInteger n = pq;

        /*Le couple (n,e) est la clé publique du chiffrement,
        alors que le nombre d est sa clé privée, sachant que l'opération de déchiffrement
        ne demande que la clef privée d et l'entier n, connu par la clé publique (la clé privée est parfois
        aussi définie comme le triplet (d, p, q).*/


        BigInteger[] publicKey = new BigInteger[3];
        publicKey[0] = n;
        publicKey[1] = e;
        
        Random rd = new Random();
        BigInteger r = new BigInteger(mess.bitLength()*rd.nextInt() , new Random());
        BigInteger gR = new BigInteger(DigestUtils.sha1(r.toByteArray()));

        publicKey[2] = gR;

        PublicKey publicKey1 = new PublicKey(publicKey);
        BigInteger[] privateKey = new BigInteger[4];
        privateKey[0]= d;
        privateKey[1]= p;
        privateKey[2]= q;
        privateKey[3]= r;

        PrivateKey privateKey1 = new PrivateKey(privateKey);
        return new Keys(publicKey1, privateKey1);
    }


    public static BigInteger encrypt(PublicKey publicKey, BigInteger toCrypt) {

        BigInteger[] publicKey1 = publicKey.getPublicKey();
        BigInteger n = publicKey1[0];
        BigInteger e = publicKey1[1];

        BigInteger cryptedMess = toCrypt.modPow(e, n);

        return cryptedMess;
    }
    
    public static BigInteger encryptWithSalt(PublicKey publicKey, BigInteger toCrypt) {

        BigInteger[] publicKey1 = publicKey.getPublicKey();
        BigInteger n = publicKey1[0];
        BigInteger e = publicKey1[1];
        BigInteger gR = publicKey1[2];

        BigInteger cryptedMess = toCrypt.modPow(e, n).xor(gR);
        
        return cryptedMess;
    }

    /***
     * @param cryptedMess le message a crypter
     * @param sk la clé secrete
     * @return le message crypté (un entier)
     */
    public static BigInteger decrypt(BigInteger cryptedMess, PrivateKey sk) {

        BigInteger p = sk.getPrivateKey()[1];
        BigInteger q = sk.getPrivateKey()[2];
        BigInteger n = p.multiply(q);

        BigInteger d = sk.getPrivateKey()[0];

        return cryptedMess.modPow(d,n);
    }

    /***
     * @param cryptedMess le message a crypter
     * @param sk la clé secrete
     * @return le message crypté (un entier)
     */
    public static BigInteger decryptWithSalt(BigInteger cryptedMess, PrivateKey sk) {

        BigInteger p = sk.getPrivateKey()[1];
        BigInteger q = sk.getPrivateKey()[2];
        BigInteger n = p.multiply(q);

        BigInteger d = sk.getPrivateKey()[0];
        BigInteger r = sk.getPrivateKey()[3];
        
        BigInteger gR = new BigInteger(DigestUtils.sha1(r.toByteArray()));
        
        return cryptedMess.xor(gR).modPow(d,n);
    }

    /***
     * @param cryptedMess le message a crypter
     * @param secretKey la clé secrete
     * @return le message crypté (un entier)
     */
    public static BigInteger decrypt_CRT(BigInteger cryptedMess, PrivateKey secretKey) {
    	return resteChinois(secretKey, cryptedMess);

    }

    public static BigInteger resteChinois(PrivateKey pk, BigInteger c) {

        //On réucpere les attributs de la clé privée
        BigInteger d = pk.getPrivateKey()[0];
        BigInteger p = pk.getPrivateKey()[1];
        BigInteger q = pk.getPrivateKey()[2];
        
    	BigInteger cp = c.mod(p);
    	BigInteger cq = c.mod(q);

    	BigInteger dp = d.mod(p.subtract(BigInteger.ONE));
    	BigInteger dq = d.mod(q.subtract(BigInteger.ONE));

        //n^p = cp^dp mod p
    	BigInteger np = cp.modPow(dp, p);
        //n^q = cq^dq mod q
    	BigInteger nq = cq.modPow(dq, q);

        //Pour les restes chinois CRT et resoudre les equations diophantiennes
    	BigInteger[] xs = new BigInteger[2];
    	BigInteger[] mis = new BigInteger[2];
    	xs[0]= np;
    	xs[1]= nq;
    	mis[0]= p;
    	mis[1]= q;
   
    	return CRT(xs,mis);
    }
    
    public static BigInteger bigAttack(BigInteger c1,BigInteger c2, BigInteger c3, BigInteger N1, BigInteger N2, BigInteger N3){
    	//On va décrypter le message sans la clé publique

        //Dans le cas de trois personnes partager le meme exposant modulaire dans la clé publique
        BigInteger[] xs = new BigInteger[3];
    	BigInteger[] mis = new BigInteger[3];;
    	
    	xs[0]= c1;
    	xs[1]= c2;    	
    	xs[2]= c3;

    	mis[0] = N1;
    	mis[1] = N2;
    	mis[2] = N3;

    	//return la racine cubique de CRT
    	return Newton.cqrt(CRT(xs,mis));
    }
    
    /*
     * Chinese Reminder Theorem
     * 
     *  Permet de résoudre les équations diophantiennes
     *  i.e. de retrouver un entier verifiant des équations de la forme
     *  res = xs[i] mod mis[i]
     * 
     */
    public static BigInteger CRT(BigInteger[] xs, BigInteger[] mis) {

          // les équations sont
         //  x = xs[i] mod mis[i] (RSA | x = xs[0] mod p  | x = xs[1] mod q)

        //   n = π mis[i] (RSA | p*q,  p = mis[0] and q = mis[1]
    	assert(xs.length == mis.length);
    	
    	BigInteger n = mis[0];
    	for(int i=1; i< mis.length; i++)
    	{
    		n = n.multiply(mis[i]);
    	}
    	
    	BigInteger res = BigInteger.ZERO;
    	
    	for (int i=0; i<mis.length ; i++){
	    		BigInteger mi = n.divide(mis[i]);
	    		BigInteger yi= mi.modInverse(mis[i]);
	    		
	    		BigInteger miyi = mi.multiply(yi);	
	    		res = (res.add(xs[i].multiply(miyi))).mod(n);
		}
    	
		return res;			
    }


}
