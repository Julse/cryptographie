package EVALUATION;

import java.math.BigInteger;

/**
 * Created by Jules on 31/03/2016.
 */
public class PublicKey {
    private BigInteger[] publicKey = new BigInteger[3];

    public PublicKey(BigInteger[] publicKey) {
        this.publicKey = publicKey;
    }

    public BigInteger[] getPublicKey() {

        return publicKey;
    }

    public void setPublicKey(BigInteger[] publicKey) {
        this.publicKey = publicKey;
    }
}
