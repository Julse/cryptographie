package EVALUATION;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class Elgamal_add {
	
	private final Random rand = new Random();
	private final int certainty = 50;

	/**
	 * @return la génération aléatoire d'une clé publique et d'une clé privée comme suit :
	 * (a) tirer aleatoirement un premier p a l'aide de la methode getPrime. Celui-ci est de la forme p = 2p'+1
	 * avec p' premier,
	 * (b) tirer aleatoirement un generateur de (Z/pZ)* d'ordre 2*p'!! ;
	 * (c) tirer aleatoirement un entier x de [0; p' - 1] ;
	 * (d) calculer h = g^x dans Z/pZ;
	 * (e) la clef secrete est le couple (p; x), la clef publique est (p; g; h).
	 */
	public Keys generateKeys() {
		BigInteger p = PrimeUtil.getPrime(128, certainty, rand); //(a)
		BigInteger pprime = p.subtract(BigInteger.ONE).divide(new BigInteger("2"));
		BigInteger g = PrimeUtil.getGeneratorOfZpZ(p);//(b) d'ordre p' : impossible, sinon ce n'est pas un générateur!!

		BigInteger x = PrimeUtil.randNum(pprime,certainty, rand);

		BigInteger h = g.modPow(x,p);

		BigInteger[] publicKey = new BigInteger[3];
		publicKey[0] = p;
		publicKey[1] = g;
		publicKey[2] = h;
		PublicKey publicKey1 = new PublicKey(publicKey);
		BigInteger[] privateKey = new BigInteger[2];
		privateKey[0]= p;
		privateKey[1]= x;
		PrivateKey privateKey1 = new PrivateKey(privateKey);

		return new Keys(publicKey1, privateKey1);
	};


	/**
	 * @param publicKey la clé publique à chiffrer
	 * @param toCrypt l'entier a crypter
	 * @return une paire (g^r;m*h^r)
	 */
	public BigInteger[] encrypt(PublicKey publicKey, BigInteger toCrypt) {
		BigInteger[] paire = new BigInteger[2];

		BigInteger[] publicKey1 = publicKey.getPublicKey();
		BigInteger p = publicKey1[0];
		BigInteger g = publicKey1[1];
		BigInteger h = publicKey1[2];
		BigInteger pprime = p.subtract(BigInteger.ONE).divide(new BigInteger("2"));

		BigInteger r = PrimeUtil.randNum(pprime,certainty,rand);

		paire[0] = g.modPow(r,p);
		//paire[1] = toCrypt.multiply(h.modPow(r, p));
		paire[1] = g.modPow(toCrypt, p).multiply(h.modPow(r, p)).mod(p);

		return paire;
	}

	/**
	 * @param paire l'entier a décrypter
	 * @return l'entier dechiffre
	 */
	public BigInteger decrypt (BigInteger[] paire, PrivateKey secretKey) {
		BigInteger gr = paire[0];
		BigInteger p = secretKey.getPrivateKey()[0];
		BigInteger x = secretKey.getPrivateKey()[1];
		
		
		BigInteger gm = paire[1].multiply((gr.modPow(secretKey.getPrivateKey()[1], p).modInverse(p))).mod(p).multiply(gr.modPow(x, p)).modInverse(p);
		
		
		BigInteger g = gr.multiply(gr.modPow(x, p)).mod(p).multiply(gr.modPow(x, p).modInverse(p));
		
		BigInteger gx = g;
		int m=0;
		while (!gx.equals(gm)){
			m++;
			gx = gx.multiply(g);
		}
		return new BigInteger(""+m);
	}
	public class Keys {
	    private PublicKey publicKey;
	    private PrivateKey privateKey;

	    public Keys(PublicKey publicKey, PrivateKey privateKey) {
	        this.publicKey = publicKey;
	        this.privateKey = privateKey;
	    }

	    public PublicKey getPublicKey() {
	        return publicKey;
	    }

	    public void setPublicKey(PublicKey publicKey) {
	        this.publicKey = publicKey;
	    }

	    public PrivateKey getPrivateKey() {
	        return privateKey;
	    }

	    public void setPrivateKey(PrivateKey privateKey) {
	        this.privateKey = privateKey;
	    }
	}
	public class PrivateKey {
	    private BigInteger[] privateKey = new BigInteger[2];

	    public PrivateKey(BigInteger[] privateKey) {
	        this.privateKey = privateKey;
	    }

	    public BigInteger[] getPrivateKey() {

	        return privateKey;
	    }

	    public void setPrivateKey(BigInteger[] privateKey) {
	        this.privateKey = privateKey;
	    }
	}
	public class PublicKey {
	    private BigInteger[] publicKey = new BigInteger[3];

	    public PublicKey(BigInteger[] publicKey) {
	        this.publicKey = publicKey;
	    }

	    public BigInteger[] getPublicKey() {

	        return publicKey;
	    }

	    public void setPublicKey(BigInteger[] publicKey) {
	        this.publicKey = publicKey;
	    }
	}


	public static class PrimeUtil {
		
		static int certainty = 50;
	    /**
	     * @param nb_bits la nombre de bits de l'entier à générer
	     * @param certainty une paramètre fluctuant la probabilité que l'entier p' soit premier
	     * @param prg un générateur de random
	     * @return un très grand entier p = 2*p'+1, à haute probabilité d'etre premier
	     */
	    public static BigInteger getPrime(int nb_bits, int certainty, Random prg)
	    {
	    	 long a = System.currentTimeMillis();
	        BigInteger bi = new BigInteger(nb_bits, certainty, prg);
	        BigInteger bi2 = new BigInteger(bi.toString());
	        bi2.add(bi);
	        bi2.add(new BigInteger("1"));

	        while (bi2.bitLength() != nb_bits && !bi2.isProbablePrime(0)){
	            bi = new BigInteger(nb_bits, certainty, prg);
	            bi2 = new BigInteger(bi.toString());
	            bi2.add(bi);
	            bi2.add(new BigInteger("1"));
	        }

	        long b = System.currentTimeMillis();
	        System.out.println("premier genere en : " + (b-a) + " ms");
	        return bi2;
	    }


	    /**
	     * @param p l'entier du groupe (Z/pZ)*
	     * @return un générateur du groupe (Z/pZ)*
	     */
	    public static BigInteger getGeneratorOfZpZ(BigInteger p){
	        BigInteger pprime = p.subtract(new BigInteger("1")).divide(new BigInteger("2"));
	        BigInteger res = pprime.subtract(new BigInteger((int) (Math.random()*pprime.bitLength()), new Random()));

	        while (res.signum() != 1){
	            res = pprime.subtract(new BigInteger((int) (Math.random()*pprime.bitLength()), new Random()));
	        }

	        res = res.nextProbablePrime().mod(p);
	        //res=res.multiply(res)

	        BigInteger g2 = res.pow(2).mod(p);
	        BigInteger gpp = res.modPow(pprime, p);

	        //Trois possibilités pour l'ordre de l'entier g généré : p', 1 ou 2*p'
	        //puisque l'ordre d'une element divise l'ordre du groupe
	        //Si l'ordre de l'entier premier genere est egale a 2 ou g, alors ce n'est pas un générateur!
	        //Car on veut g^i autant que d'element du groupe!
	        while (g2.equals(BigInteger.ONE) || gpp.equals(BigInteger.ONE)){
	            res = res.nextProbablePrime().mod(p);
	            g2 = res.pow(2).mod(p);
	            gpp = res.modPow(pprime, p);
	        }
	        System.out.println("g : " + res);
	        System.out.println("g2 : " + g2); /* element d'ordre p */ /* toujours different de 1 */
	        System.out.println("gpp : " + gpp); /* element d'ordre 2 */ /* toujours different de 1 */

	        BigInteger g2pp = gpp.modPow(new BigInteger("2"), p);
	        System.out.println("g2pp : " + g2pp); /* forcément égal a l'element neutre 1 */

	        System.out.println("");

	        return res;
	    }


	    /**
	     * Inscrit dans un fichier texte la liste des entiers du groupe (Z/pZ)* avec leur ordre
	     * @param p l'entier p du groupe (Z/pZ)*, p est premier tel que p=2p'+1
	     */
	    public static void ordre_p(BigInteger p){

	        try {

	            File file = new File(("Z." + p.toString() +"Z" + ".txt"));

	            //File file = new File("/Z." + p.toString() +"Z" + ".txt");
	            if (!file.exists()) {
	                file.createNewFile();
	            }

	            FileWriter fw = new FileWriter(file.getAbsoluteFile());
	            BufferedWriter bw = new BufferedWriter(fw);

	            bw.write("Ordre des elements du groupe Z/" + p.toString() + "Z :\n");

	            if (p.equals(BigInteger.ZERO)){
	                return;
	            }

	            BigInteger pprime = p.subtract(new BigInteger("1")).divide(new BigInteger("2"));
	            BigInteger i = new BigInteger("1");
	            while (!(i.equals(p))) {
	                if (i.modPow(new BigInteger("2"),p).equals(BigInteger.ONE)){
	                    bw.write("Ordre de " + i.toString() + " : " + 2 + "\n");
	                }
	                else if (i.modPow(pprime,p).equals(BigInteger.ONE)){
	                    bw.write("Ordre de " + i.toString() + " : " + pprime.toString() + "\n");
	                }
	                else {
	                    bw.write("Ordre de " + i.toString() + " : " + p.subtract(BigInteger.ONE).toString() + "\n");
	                }
	                i = i.add(BigInteger.ONE);
	            }

	            bw.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    /**
	     * Inscrit dans un fichier texte la liste des entiers du groupe (Z/pZ)* avec leur ordre
	     * @param p l'entier p du groupe (Z/pZ)*
	     */
	    public static void ordre(BigInteger p){

	        try {
	            File file = new File(("Z." + p.toString() +"Z" + ".txt"));

	            if (!file.exists()) {
	                file.createNewFile();
	            }

	            FileWriter fw = new FileWriter(file.getAbsoluteFile());
	            BufferedWriter bw = new BufferedWriter(fw);

	            bw.write("Ordre des elements du groupe Z/" + p.toString() + "Z :\n");
	            System.out.println("Ordre des elements du groupe Z/" + p.toString() + "Z :");

	            if (p.equals(BigInteger.ZERO)){
	                return;
	            }
	            ArrayList<BigInteger> zpzetoile = new ArrayList<BigInteger>();
	            BigInteger i = new BigInteger("1");
	            while (!(i.equals(p))) {
	                if ((p.gcd(i)).equals(new BigInteger("1"))) {
	                    zpzetoile.add(i);
	                }
	                i = i.add(BigInteger.ONE);
	            }

	            for (BigInteger bi : zpzetoile) {
	                System.out.print("Ordre de " + bi.toString() + " : ");
	                bw.write("Ordre de " + bi.toString() + " : ");
	                int j = 1;
	                while (!(bi.pow(j).mod(p).equals(new BigInteger("1")))) {
	                    j++;
	                    while (zpzetoile.size()%j != 0) {
	                        j++;
	                    }
	                }
	                System.out.println(j);
	                bw.write(j +"\n");
	            }

	            System.out.println("");
	            bw.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    /**
	     * @param n un grand entier
	     * @return la racine de l'entier n
	     */
	    public static BigInteger sqrt(BigInteger n) {
	        BigInteger a = BigInteger.ONE;
	        BigInteger b = new BigInteger(n.shiftRight(5).add(new BigInteger("8")).toString());
	        while(b.compareTo(a) >= 0) {
	            BigInteger mid = new BigInteger(a.add(b).shiftRight(1).toString());
	            if(mid.multiply(mid).compareTo(n) > 0) b = mid.subtract(BigInteger.ONE);
	            else a = mid.add(BigInteger.ONE);
	        }
	        return a.subtract(BigInteger.ONE);
	    }

	    /**
	     * @param N
	     * @return un grand entier compris entre 0 et N-1
	     */
	    public static BigInteger randNum(BigInteger N, int certainty, Random prg){

	        int rdmInt = prg.nextInt();
	        while (rdmInt > N.bitLength()){
	            rdmInt = prg.nextInt();
	        }

	        BigInteger rdmBigInt = getPrime(N.bitLength(), certainty, prg);
	        if (N.subtract(rdmBigInt).signum() <=0){
	            rdmBigInt = getPrime(N.bitLength(), certainty, prg);
	        }
	        return rdmBigInt;
	    }
	    
		public static byte[] concat(byte[] a, byte[] b) {
			   int aLen = a.length;
			   int bLen = b.length;
			   byte[] c= new byte[aLen+bLen];
			   System.arraycopy(a, 0, c, 0, aLen);
			   System.arraycopy(b, 0, c, aLen, bLen);
			   return c;
			}
	}

	
	
    public static void main( String[] args )
    {

        Elgamal_add elgamal = new Elgamal_add();
        Keys keys = elgamal.generateKeys();

        System.out.println("Entier non crypté : " + 10);
        BigInteger[] crypt = elgamal.encrypt(keys.getPublicKey(), new BigInteger("10"));
        System.out.println("Entier crypté : " + crypt[1]);

        BigInteger decrypt = elgamal.decrypt(crypt, keys.getPrivateKey());
        System.out.println("Entier decrypté : " + decrypt);
    }
}
