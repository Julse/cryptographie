package EVALUATION;

import java.math.BigInteger;

/**
 * Created by Jules on 31/03/2016.
 */
public class PrivateKey {
    private BigInteger[] privateKey = new BigInteger[2];

    public PrivateKey(BigInteger[] privateKey) {
        this.privateKey = privateKey;
    }

    public BigInteger[] getPrivateKey() {

        return privateKey;
    }

    public void setPrivateKey(BigInteger[] privateKey) {
        this.privateKey = privateKey;
    }
}
